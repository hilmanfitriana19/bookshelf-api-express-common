const app = require('./src/app')
const env = require('./src/configs/env')
const { connectDB } = require('./src/databases')

connectDB()

const port = env.PORT

app.listen(port, () => {
	console.log(`Listen To Port ${port}`)
})
