const Joi = require("joi")

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d ]{8,}$/

const userValidator = ({
    create: Joi.object().keys({
        name: Joi.string().required()
            .messages({
                'string.base': 'name must be string',
                'string.empty': 'name cannot be empty',
                'any.required': 'name is required'
            }),
        email: Joi.string().required()
            .pattern(emailRegex)
            .messages({
                'string.base': 'email must be string',
                'string.empty': 'email cannot be an empty',
                'string.pattern.base': 'email is invalid',
                'any.required': 'email is required'
            }),
        password: Joi.string().required()
            .pattern(passwordRegex)
            .messages({
                'string.base': 'password must be string',
                'string.empty': 'password cannot be an empty',
                'string.min': 'password must have a minimum length of {#limit}',
                'string.pattern.base': 'password must include minimum eight characters, at least one letter and one number',
                'any.required': 'password is required'
            })
    }),
    update: Joi.object().keys({
        name: Joi.string().optional()
            .messages({
                'string.base': 'name must be string',
                'string.empty': 'name cannot be empty'
            }),
        email: Joi.string()
            .pattern(emailRegex)
            .messages({
                'string.base': 'email must be string',
                'string.empty': 'email cannot be an empty',
                'string.pattern.base': 'email is invalid'
            }),
        password: Joi.string()
            .pattern(passwordRegex)
            .messages({
                'string.empty': 'password cannot be an empty',
                'string.min': 'password must have a minimum length of {#limit}',
                'string.pattern.base': 'password must include minimum eight characters, at least one letter and one number',
            })
    }),
    login: Joi.object().keys({
        email: Joi.string().required()
            .pattern(emailRegex)
            .messages({
                'string.base': 'email must be string',
                'string.empty': 'email cannot be an empty',
                'string.pattern.base': 'email is invalid',
                'any.required': 'email is required'
            }),
        password: Joi.string().required()
            .messages({
                'string.base': 'password must be string',
                'string.empty': 'password cannot be an empty',
                'any.required': 'password is required'
            })
    }),
})

module.exports = userValidator