const Joi = require("joi")

const queryValidator = Joi.object().keys({
    offset: Joi.number().optional().integer().allow(null).allow('')
        .messages({
            'number.base': 'offset must be number'
        }),
    limit: Joi.number().optional().integer().allow(null).allow('')
        .messages({
            'number.base': 'limit must be number'
        }),

})

module.exports = queryValidator