const Joi = require("joi")

const userBookValidator = ({
    create: Joi.object().keys({
        reader: Joi.string().required()
            .messages({
                'string.base': 'reader must be string',
                'string.empty': 'reader cannot be empty',
                'any.required': 'reader is required'
            }),
        book: Joi.string().required()
            .messages({
                'string.base': 'book must be string',
                'string.empty': 'book cannot be empty',
                'any.required': 'book is required'
            }),
        readPage: Joi.number().optional()
            .messages({
                'number.base': 'readPage must be number',
            }),

    }),
    update: Joi.object().keys({
        reader: Joi.string().optional()
            .messages({
                'string.base': 'reader must be string',
                'string.empty': 'reader cannot be empty',
            }),
        book: Joi.string().optional()
            .messages({
                'string.base': 'book must be string',
                'string.empty': 'book cannot be empty',
            }),
        readPage: Joi.number().optional()
            .messages({
                'number.base': 'readPage must be number',
            }),
    }),
})


module.exports = userBookValidator