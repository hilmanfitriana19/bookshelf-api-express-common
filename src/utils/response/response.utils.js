const { ResponseCode } = require("../contants/response.code.constant");
const { ResponseMessage } = require("../contants/response.message.contant");

const responseUtil = ({
    SuccessCreated: (res, data) => {
        res.status(ResponseCode.CREATED).json({
            "message": ResponseMessage.SuccessCreated,
            data
        });
    },

    SuccessProcess: (res, message, data) => {
        res.status(ResponseCode.SUCCESS).json({
            "message": message || ResponseMessage.SuccessProcess,
            data
        });
    },

    NotFoundData: (res, message) => {
        res.status(ResponseCode.NOT_FOUND).json({ "error": message || ResponseMessage.DataNotFound });
    },

    FailProcess: (res, message) => {
        res.status(ResponseCode.FAIL).json({ "error": message || ResponseMessage.FailProcess });
    },

    UnauthorizedAccess: (res, message) => {
        res.status(ResponseCode.UNAUTHORIZED).json({ "error": message || ResponseMessage.FailProcess });
    },
    InternalServerError: (res) => {
        res.status(ResponseCode.INTERNAL_SERVER_ERROR).json({ 'error': ResponseMessage.InternalServerError })
    }

})

module.exports = responseUtil