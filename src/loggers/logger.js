const winston = require("winston");
const winstonDailyRotateFile = require("winston-daily-rotate-file");
const env = require("../configs/env");

const logger = winston.createLogger({
    exitOnError: false,
    level: 'http',
    transports: [
        new winston.transports.Console(),
        new winstonDailyRotateFile({
            dirname: env.LOGGER_PATH + '/app/combine',
            filename: 'combine-%DATE%',
            extension: '.log',
            level: 'info',
            zippedArchive: true,
            handleExceptions: true,
            datePattern: 'YY-MM-DD',
            maxSize: '20m',
            maxFiles: '14d',

        }),
        new winstonDailyRotateFile({
            dirname: env.LOGGER_PATH + '/app/error',
            filename: 'error-%DATE%',
            extension: '.log',
            level: 'error',
            zippedArchive: true,
            datePattern: 'YY-MM-DD',
            maxSize: '20m',
            maxFiles: '14d'
        }),
    ],
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.printf(info => `[${info.timestamp}] - [${info.level}] - ${info.message}`)
    ),
});


module.exports = logger