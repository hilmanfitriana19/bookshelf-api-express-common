const bcrypt = require('bcrypt')

exports.encodePassword = async(password) => {
    const salt = await bcrypt.genSalt()
    return await bcrypt.hash(password,salt)
}

exports.isCorrectPassword = (rawPassword,hash) => {
    return bcrypt.compareSync(rawPassword,hash)
}