exports.getMaxAge = (value) => {
    const number = parseInt(value)
    const type = value.slice(-1)

    switch (type) {
        case 'd':
            return number * 24 * 60 * 60 * 1000;
        case 'h':
            return number * 60 * 60 * 1000;
        case 'm':
            return number * 60 * 1000;
        case 's':
            return number * 1000;
        case 'y':
            return number * 365 * 24 * 60 * 60 * 1000;
        default:
            return 0;
    }

}
