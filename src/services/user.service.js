const { encodePassword } = require("../helpers/password.helper")
const User = require("../models/user.model")

const userService = {}

userService.save = async (body) => {
    const newBody = { ...body }
    newBody.password = await encodePassword(body.password)
    const user = new User(newBody);
    return user.save();
}

userService.getAll = (offset,limit) => {
    return User.find()
    .populate({
        path: 'books',
        select: ['readPage'],
        populate: {
            path: 'book',
            select: ['_id', 'name'],
        },
        transform: function (data) {
            return (data.book!=null)?{
                id: data.book.id,
                name: data.book.name,
                readPage: data.readPage
            }: []
        }
    })
    .skip(offset || 0).limit(limit || null);

}

userService.getOne = (id) => {
    return User.findById(id).select('-books').orFail();
}

userService.getOneByEmail = (email) =>{
    return User.findOne({ email });
}

userService.update = async (id,body) => {
    if (body?.password)
            body.password = await encodePassword(body?.password)
        return User.findOneAndUpdate({ _id: id }, body, { returnOriginal: false, select: '-books' });

}

userService.delete = (id) => {
    return User.findByIdAndDelete({ _id: id }, { returnOriginal: false, select: '-books' });
}

module.exports = userService