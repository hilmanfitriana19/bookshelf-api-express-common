const Book = require("../models/book.model");

const bookService = {}

bookService.save = (body) => {
    const book = new Book(body);
    return book.save();
},

bookService.getAll = (offset, limit) => {
    return Book.find().select('-users').skip(offset || 0).limit(limit || null);
},

bookService.getOne = (id) => {
    return Book.findById(id).select('-users').orFail();
},

bookService.update = (id, body) => {
    return Book.findOneAndUpdate({ _id: id }, body, { returnOriginal: false, select: '-users' })
},

bookService.delete = (id) => {
    return Book.findOneAndDelete({ _id: id }, { select: '-users' })
}

module.exports = bookService

