const Book = require('../models/book.model');
const User = require('../models/user.model');
const UserBook = require('../models/userBook.model');

const userBookService = {}

userBookService.save = async (body) => {
    const userBook = new UserBook(body);
    const result = await userBook.save();
    await Book.findByIdAndUpdate(userBook.book, { $push: { users: userBook._id } })
    await User.findByIdAndUpdate(userBook.reader, { $push: { books: userBook._id } })
    return result
},

userBookService.getAll =  (offset, limit) => {
    return UserBook.find().skip(offset || 0).limit(limit || null);
},

userBookService.getOneById = (id) => {
    return UserBook.findById(id).orFail();
},

userBookService.update =  async (id, body) => {
    const userBook = await UserBook.findById({ _id: id });

    if ('reader' in body) {
        if (userBook.reader.toString() !== body.reader) {
            await User.findByIdAndUpdate(userBook.reader, { $pull: { books: userBook._id } })
            await User.findByIdAndUpdate(body.reader, { $push: { books: userBook._id } })
        }
    }

    if ('book' in body) {
        if (userBook.book.toString() !== body.book) {
            await Book.findByIdAndUpdate(userBook.book, { $pull: { users: userBook._id } })
            await Book.findByIdAndUpdate(body.book, { $push: { users: userBook._id } })
        }
    }
    return UserBook.findOneAndUpdate({ _id: id }, body, { returnOriginal: false })
},

userBookService.delete = async (id) => {
    const userBook = await UserBook.findOneAndDelete({ _id: id });
    await Book.findByIdAndUpdate(userBook.book, { $pull: { users: userBook._id } })
    await User.findByIdAndUpdate(userBook.reader, { $pull: { books: userBook._id } })
    return userBook
}

module.exports = userBookService