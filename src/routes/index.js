const express = require('express')
const authController = require('../controllers/auth.controller')
const bookController = require('../controllers/book.controller')
const userController = require('../controllers/user.controller')
const userBookController = require('../controllers/userBook.controller')
const { JwtAuth } = require('../middlewares/authentication')
require('express-group-routes')

const router = express()

router.group('/users', (router) => {
    router.post('/', JwtAuth,userController.create)
    router.get('/', userController.getAll)
    router.get('/:id',userController.getOne)
    router.patch('/:id', JwtAuth, userController.update)
    router.delete('/:id',JwtAuth, userController.delete)
})

router.group('/books', (router) => {
    router.post('/', JwtAuth, bookController.create)
    router.get('/', bookController.getAll)
    router.get('/:id', bookController.getOne)
    router.patch('/:id', JwtAuth, bookController.update)
    router.delete('/:id', JwtAuth, bookController.delete)
})


router.group('/user-books', (router) => {
    router.post('/',JwtAuth,userBookController.create)
    router.get('/', JwtAuth, userBookController.getAll)
    router.get('/:id',JwtAuth, userBookController.getOne)
    router.patch('/:id',JwtAuth, userBookController.update)
    router.delete('/:id',JwtAuth, userBookController.delete)
})

router.group('/auth', (router) => {
    router.post('/login', authController.login)
    router.post('/refresh', authController.refreshToken)
    router.post('/logout', authController.logout)
})


module.exports = { router }
