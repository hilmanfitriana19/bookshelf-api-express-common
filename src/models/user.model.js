const { default: mongoose, Schema } = require("mongoose");

const UserSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    currentRefreshToken: String,
    books: [{
        type: Schema.Types.ObjectId,
        ref: "UserBook"
    }]

},
    { timestamps: true }
)


UserSchema.method('toJSON', function () {
    const { __v, _id, createdAt, updatedAt, password, currentRefreshToken, ...object } = this.toObject();
    object.id = _id;
    return object;
})

const User = mongoose.model('User',UserSchema)

module.exports = User