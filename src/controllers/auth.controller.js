const logger = require("../loggers/logger");
const authService = require("../services/auth.service");
const { objectToStr } = require("../utils/logs/log.utils");
const responseUtil = require("../utils/response/response.utils");

const authController = ({
    login: async(req,res) => {
        try{
            const validatorResult = userValidator.login.validate(req.body, { allowUnknown: true })
            if (validatorResult.error) {
                logger.error(`Request Create User Data Failed ${validatorResult.error.message}`);
                return responseUtil.FailProcess(res, validatorResult.error.message);
            }

            logger.info(`Login Attempt Occur using : ${objectToStr(validatorResult.value)}`)

            const result = await authService.login(req.body)
            if (result) {
                logger.info(`Login Attempt Success with credential email : ${objectToStr({ email: req?.body.email })}`)
                res.setHeader('Set-Cookie', [result.accessTokenCookie, result.refreshTokenCookie])
                return responseUtil.SuccessProcess(res, ResponseMessage.LoginSucess)
            } else {
                logger.error(`Login Attempt Failed with email ${objectToStr({ email: req?.body.email })}`)
                return responseUtil.UnauthorizedAccess(res, ResponseMessage.LoginFailed)

            }
        } catch (e) {
            logger.error(`Failed login :  ${e}`);
            return responseUtil.InternalServerError(res)

        }
    },

    refreshToken: async(req,res) => {
        logger.info(`Request New Access Token `)
        const refreshToken = req.cookies.Refresh

        if (!refreshToken) {
            logger.info(`Request New Access Token Failed, No Refresh Token in Request `)
            return responseUtil.UnauthorizedAccess(res, ResponseMessage.NoToken)
        }

        try {
            const result = await authService.refresh(refreshToken);
            if (result) {
                res.setHeader('Set-Cookie', [result])
                return responseUtil.SuccessProcess(res, ResponseMessage.SuccessRefreshToken)
            } else {
                return responseUtil.UnauthorizedAccess(res, ResponseMessage.InvalidToken)
            }
        } catch (e) {
            logger.error(`Failed Generated New Access Token : ${e}`)
            if (e?.message === 'jwt expired')
                return responseUtil.UnauthorizedAccess(res, ResponseMessage.TokenExpired)

            return responseUtil.UnauthorizedAccess(res, ResponseMessage.InvalidToken)
        }
    },

    logout: async (req, res) => {

        logger.info(`Request Logout `)
        const refreshToken = req.cookies.Refresh

        try {
            const result = await authService.logout(refreshToken);
            res.setHeader('Set-Cookie', [result.accessTokenCookie, result.refreshTokenCookie])
             return responseUtil.SuccessProcess(res, ResponseMessage.LogoutSuccess)
        } catch (e) {
            logger.error(`Failed To Logout ${e}`)
            return responseUtil.FailProcess(res, 'Failed to Logout')
        }
    }

})

module.exports = authController