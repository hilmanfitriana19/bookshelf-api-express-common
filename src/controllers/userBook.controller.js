const { default: mongoose } = require("mongoose")
const logger = require("../loggers/logger")
const userBookService = require("../services/userBook.service")
const { ResponseMessage } = require("../utils/contants/response.message.contant")
const responseUtil = require("../utils/response/response.utils")
const queryValidator = require("../utils/validators/query.validator")
const userBookValidator = require("../utils/validators/user-book.validator")

const userBookController = ({
    create: async (req, res) => {
        const body = req.body

        logger.info(`Request create user book data with body ${JSON.stringify(body)}`)
        const validatorResult = userBookValidator.create.validate(body, { allowUnknown: true })

        if (validatorResult.error) {
            logger.error(`Request Create User Book Data Failed ${validatorResult.error.message}`)
            return responseUtil.FailProcess(res, validatorResult.error.message);
        }

        try{
            await userService.getOneById(body.reader)
            await bookService.getOneById(body.book)

            const data = await userBookService.save(body)
            logger.info(`Successfuly create user book data`)
            return responseUtil.SuccessCreated(res, data)
        } catch (e) {

            logger.error(`Request Create User Data Failed : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError) {
                const model = (e?.message.includes('Book')) ? 'Book' : 'User'
                return responseUtil.NotFoundData(res, `Data ${model} Not Found`)
            }

            if (e.code === MongoErrorCode.DUPLICATE) return responseUtil.FailProcess(res, ResponseMessage.DuplicateUserBook)
            return responseUtil.InternalServerError(res)
        }

    },

    getAll: async (req, res) => {
        const {offset,limit} = req.query;

        logger.info(`Request Get List Book start from index ${offset || 0} limit : ${limit || '-'} `)

        const validationResult = queryValidator.validate({ offset, limit }, { allowUnknown: true })
        if (validationResult.error) {
            logger.error(`Fail Get List User Book : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res, validationResult.error.message);
        }

        const result = await userBookService.getAll(offset,limit)
        return res.send(result);

    },

    getOne: async (req, res) => {
        const { id } = req.params;
        logger.info(`Request Get User Book data with id : ${id}`)
        
        try{
            const result = await userBookService.getOne(id)
            return res.send(result)
        }catch(e){
            logger.error(`Request Get One Failed : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)   
        }
    },

    update: async (req, res) => {
        const {id} = req.params;
        const body = req.body;

        logger.info(`Request Update User Book Data with id ${id} with body ${JSON.stringify(body)}`)
        const validatorResult = userBookValidator.update.validate(body, { allowUnknown: true })

        if (validatorResult.error) {
            logger.error(`Request Update User Book Data Failed ${validatorResult.error.message}`)
            return responseUtil.FailProcess(res, validatorResult.error.message);
        }


        try {
            await userBookService.getOne(id)

            if (body.reader) await userService.getOne(body.reader)
            if (body.book) await bookService.getOne(body.book)

            const data = await userBookService.update(id, body)
            logger.info(`Successfully update user book data with id ${id}`)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessUpdated, data)

        } catch (e) {
            logger.error(`Request Update Data id ${id} Failed : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError) {
                if (e?.message.includes('on model "UserBook"'))
                    return responseUtil.NotFoundData(res)
                const model = (e?.message.includes('Book')) ? 'Book' : 'User'
                return responseUtil.NotFoundData(res, `Data ${model} Not Found`)
            }
            return responseUtil.InternalServerError(res)
        }
    },

    delete: async (req, res) => {
        const { id } = req.params
        logger.info(`Request to Delete User Book Data with id : ${id}`)
    
        try {
            await userBookService.getOne(id);
            const data = await userBookService.delete(id);
            logger.info(`Successfully delete user book data with id ${id}`)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessDeleted, data)

        } catch (e) {
            logger.error(`Request Delete Data id ${id} Failed : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    }
})

module.exports = userBookController
