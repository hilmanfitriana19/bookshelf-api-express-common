const { default: mongoose } = require("mongoose")
const logger = require("../loggers/logger")
const userService = require("../services/user.service")
const { MongoErrorCode } = require("../utils/contants/mongodb.code.constant")
const { ResponseMessage } = require("../utils/contants/response.message.contant")
const { objectToStr } = require("../utils/logs/log.utils")
const responseUtil = require("../utils/response/response.utils")
const queryValidator = require("../utils/validators/query.validator")
const userValidator = require("../utils/validators/user.validator")

const userController = ({
    create: async (req, res) => {
        const body = req.body
        logger.info(`Request Create Data User with body ${objectToStr(body)}`)
        const validatorResult = userValidator.create.validate(body, { allowUnknown: true })
        if (validatorResult.error) {
            logger.error(`Request Create User Data Failed ${validatorResult.error.message}`)
            return responseUtil.FailProcess(res, validatorResult.error.message);
        }

        try{
            const data = await userService.save(body)
            logger.info('Successfully create user data')
            delete data._doc.books
            return responseUtil.SuccessCreated(res, data)
        }catch(e){
            logger.error(`Request Create New User Failed : ${e?.message || e}`)
            if (e.code === MongoErrorCode.DUPLICATE)
                return responseUtil.FailProcess(res, ResponseMessage.EmailTaken)
                return responseUtil.InternalServerError(res)
        }
    },

    getAll: async (req, res) => {
        const {offset,limit} = req.query;
        logger.info('Request Get List User Data')
        
        const validationResult = queryValidator.validate({ offset, limit }, { allowUnknown: true })
        if (validationResult.error) {
            logger.error(`Request Get List User Failed : ${validationResult.error.message}`)
            return responseUtil.FailProcess(res, validationResult.error.message);
        }
        const result = await userService.getAll(offset,limit)
        return res.send(result);
    },

    getOne: async (req, res) => {
        const { id } = req.params
        logger.info(`Request Get User Data with id : ${id}`)

        try{
            const result = await userService.getOne(id)
            return res.send(result)
        }catch(e){
            logger.error(` Failed to get User Data with id ${id} : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    },

    update: async (req, res) => {
        const {id} = req.params;
        const body = req.body;
        logger.info(`Request to Update Data User with id : ${id}`)

        const validatorResult = userValidator.update.validate(body, { allowUnknown: true })

        if (validatorResult.error) {
            logger.error(`Request Update User Data Failed ${validatorResult.error.message}`)
            return responseUtil.FailProcess(res, validatorResult.error.message);
        }

        try {
            await userService.getOne(id);
            const data = await userService.update(id, body)
            logger.info(`Successfully Update User Data with id : ${id}`)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessUpdated, data)

        } catch (e) {
            logger.error(`Failed to Update Data User with id ${id} : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)

            if (e.code === MongoErrorCode.DUPLICATE)
                return responseUtil.FailProcess(res, ResponseMessage.EmailTaken)

            return responseUtil.InternalServerError(res)
        }
    },

    delete: async (req, res) => {
        const { id } = req.params
        logger.info(`Request to Delete User with id : ${id}`)
        
        try {
            await userService.getOne(id);
            const data = await userService.delete(id);
            logger.info(`Successfully delete user data with id : ${id}`)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessDeleted, data)
        } catch (e) {
            logger.error(`Failed to Delelete User data with id ${id} : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    }
})

module.exports = userController
