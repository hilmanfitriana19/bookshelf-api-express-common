const { default: mongoose } = require("mongoose");
const logger = require("../loggers/logger");
const bookService = require("../services/book.service");
const { MongoErrorCode } = require("../utils/contants/mongodb.code.constant");
const { ResponseMessage } = require("../utils/contants/response.message.contant");
const responseUtil = require("../utils/response/response.utils");
const bookValidator = require("../utils/validators/book.validator");
const queryValidator = require("../utils/validators/query.validator");

const bookController = ({
    create: async (req, res) => {
        const body = req.body
        try{

            logger.info(`Request Create New Book Data with data ${JSON.stringify(body)}`)
            const validationResult = bookValidator.create.validate(body, { allowUnknown: true });
            if (validationResult.error) {
                logger.error(`Fail Create Book : ${validationResult.error.message}`);
                return responseUtil.FailProcess(res, validationResult.error.message);
            }

            const book = await bookService.save(body)
            logger.info(`Create Book Data Success`)
            return responseUtil.SuccessCreated(res, book);
        } catch (e) {
            logger.error(`Request Create New Book Data Failed : ${e?.meesage || e}`)
            if (e.code === MongoErrorCode.DUPLICATE) return responseUtil.FailProcess(res, ResponseMessage.NameBookTaken)
            return responseUtil.InternalServerError(res)
        }

    },

    getAll: async (req, res) => {
        const {offset,limit} = req.query;

        logger.info(`Request Get List Book start from index ${offset || 0} limit : ${limit || '-'} `)
        
        const validationResult = queryValidator.validate({ offset, limit }, { allowUnknown: true })
        if (validationResult.error) {
            logger.error(`Fail Get List Book : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res, validationResult.error.message);
        }
        const result = await bookService.getAll(offset,limit)
        return res.send(result);

    },

    getOne: async (req, res) => {
        const { id } = req.params;
        logger.info(`Request Get Book Data with id : ${id} `)
        
        try{
            const result = await bookService.getOne(id)
            logger.info(`Request Get Book Data Success with id: ${id} Success`)
            return res.send(result)
        }catch(e){
            logger.error(`Request Create New Book Data Failed:${e?.meesage || e} `)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    },

    update: async (req, res) => {
        const {id} = req.params;
        const body = req.body;
        try {
            logger.info(`Request Update Book Data with id : ${id} with data ${JSON.stringify(body)}`)

            const validationResult = bookValidator.update.validate(body, { allowUnknown: true });
            if (validationResult.error) {
                logger.error(`Fail Update Book : ${validationResult.error.message}`);
                return responseUtil.FailProcess(res, validationResult.error.message);
            }

            await bookService.getOne(id);
            const book = await bookService.update(id, body);
            logger.info(`Request Update Book Data Success id: ${id} `)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessUpdated, book)
        } catch (e) {
            logger.error(`Request Update Book Data Failed:${e?.meesage || e} `)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            if (e.code === MongoErrorCode.DUPLICATE)
                return responseUtil.FailProcess(res, ResponseMessage.NameBookTaken)
            return responseUtil.InternalServerError(res)
        }
    },

    delete: async (req, res) => {
        const { id } = req.params;
        logger.info(`Request Delete Book Data with id : ${id} `)
        try {
            await bookService.getOne(id);

            const book = await bookService.delete(id);

            logger.info(`Request Delete Book Data Success id: ${id} `)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessDeleted, book)

        } catch (e) {
            logger.error(`Request Delete Book Data Failed:${e?.meesage || e} `)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }


    }
})

module.exports = bookController
