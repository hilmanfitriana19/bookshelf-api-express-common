const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const env = require("../configs/env");

exports.JwtAuth = async (req,res,next) => {
    try {
        const accessToken = req.cookies.Authentication;
        const decoded = jwt.verify(accessToken, env.JWT_ACCESS_KEY);
        const user = await User.findById(decoded.user_id)
        if (!user) {
            return res.status(401).json({message:'Unauthorized Access'})
        }
        req.user_id = user._id.toString()
        next()

    } catch (e) {
        console.log(e)
        if (e instanceof jwt.JsonWebTokenError && e?.message === `jwt must be provided`) {
            return res.status(401).json({message:'Unauthorized Access'})
        } else if (e?.message === 'jwt expired') {
            return res.status(401).json({message:'Unauthorized Access'})
        }
        return res.status(401).json({message:'Unauthorized Access'})
    }

}