const env = require("./env");

module.exports = {
    dbUrl: `${env.DB_TYPE}://${env.DB_HOST}:${env.DB_PORT}/${env.DB_NAME}`,
    dbOptions: {
        authSource: env.DB_AUTH_SOURCE,
        user: env.DB_USERNAME,
        pass: env.DB_PASSWORD,
    }
}