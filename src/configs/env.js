require('dotenv').config();

module.exports = {
    PORT: process.env.PORT,
    
    DB_TYPE: 'mongodb',
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_NAME: process.env.DB_NAME,
    DB_USERNAME: process.env.DB_USERNAME,
    DB_PASSWORD: process.env.DB_PASSWORD,
    DB_AUTH_SOURCE: process.env.DB_AUTH_SOURCE,

    JWT_ACCESS_KEY: process.env.JWT_ACCESS_TOKEN_KEY,
    JWT_ACCESS_EXPIRATION_TIME: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME,

    JWT_REFRESH_KEY: process.env.JWT_REFRESH_TOKEN_KEY,
    JWT_REFRESH_EXPIRATION_TIME: process.env.JWT_REFRESH_TOKEN_EXPIRATION_TIME,

    LOGGER_PATH: process.env.LOGGER_PATH,


}