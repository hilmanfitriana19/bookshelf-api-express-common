const { default: mongoose } = require("mongoose")
const { dbUrl, dbOptions } = require("../configs/db.config")

const connectDB = () => {
    mongoose.connect(dbUrl,dbOptions
        ).then(()=>{
        console.log('Connect to Database Success')
    }).catch((e)=>{
        console.log('Connect to Database Failed ',e)
    })
}

module.exports = { connectDB }