# Bookshelf API

Aplikasi backend sederhana yang menyediakan API untuk menambah, mengubah, menampilkan dan menghapus data buku. Aplikasi dibangun menggunakan Node JS express library dengan pendekatan CommonJS.
Data tersimpan dalam database MongoDB dengan menggunakan Monggose untuk terhubung dengan Express. 

Source : Membuat Aplikasi Back-End untuk Pemula](https://www.dicoding.com/academies/261)

## Technology

- Node JS 16.19.0
- npm 8.15.0
- mongoDB

### Installation

```bash
$ npm install
```

## Running the app

```bash


$ cp .env.sample .env #  Copy .env-example and modify the .
$ npm run start   # make sure you have installed mongo

# production mode
$ npm run start:prod

```

## API Endpoint

## Auth

#### Login
Login to get JWT token with `username` and `password` as request body.

```http
POST /auth/
```


## Books 

#### Get all books
Get all data from book. Optional request can be add with offset to indicate index data in db and limit to indicate count of data as result.
```http
GET /books/
GET /books/?offset=0&&limit=2
```

#### Get book 
Get specific data from book by id
```http
GET /books/${id}
```

#### Add book
Create book data to the system. 
```http
POST /books/
```
#### Parameter
| Key     |  Description                       |
| :-------- | :-------------------------------- |
| `name` |  name of the book |
| `year` |   year of the book publisher |
| `author` | author of the book |
| `summary` | summary of the book |
| `publisher` | publisher of the book |
| `pageCount` | number indicates how many page in the book |

#### PATCH book 
Update book data with specific id. The request body has same structure with the POST method.
```http
PATCH /books/${id}
```

#### DELETE book 
Delete book data with specific id.
```http
DELETE /books/${id}
```

## User

#### Get all User
Get all data from user. Optional request can be add with offset to indicate index data in db and limit to indicate count of data as result.
```http
GET /users/
GET /users/?offset=3
GET /users/?limit=6
GET /users/?offset=3&&limit=6
```

#### Get User
Get specific data from database by user id
```http
GET /users/${id}
```

#### Add User
Create user data to the system.
```http
POST /users/
```
#### Parameter
| Key       |
| :--------- |
| `name` |
| `username` |
| `password` |
| `email` |

#### PATCH User 
Update user data with specific id. The request body has same structure with the POST method.
```http
PATCH /users/${id}
```

#### DELETE User 
Delete user data with specific id.
```http
DELETE /user/${id}
```


## User Book

#### Get all User Book
Get all data from user book that defines data that has relation between user and book. Optional request can be add with offset to indicate index data in db and limit to indicate count of data as result.
```http
GET /user-books/
GET /user-books/?offset=3
GET /user-books/?limit=6
GET /user-books/?offset=3&&limit=6
```

#### Get User Book
Get specific data from database by userBookId
```http
GET /user-books/${userBookId}
```

#### Add User Book
Create user book data.
```http
POST /user-books/
```
#### Parameter
| Key       |  Description |
| :--------- | :------------- |
| `book` | id of book |
| `reader` | username |
| `readPage` | define page being read |
| `finished` | define status of finished read |
| `reading` | defines status of reading book|


#### PATCH User Book
Update user book data with specific userBookId. The request body has same structure with the POST method.
```http
PATCH /user-books/${userBookId}
```

#### DELETE User Book
Delete user book data with specific userBookId.
```http
DELETE /user-books/${userBookId}
```
